<?php 
/* Author : Amir Mufid */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class c_api extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{
		echo "<h1>- API Portal Mahasiswa - </h1>";
		
		echo "<h3>Jadwal Perkuliahan , Kartu Rencana Studi, dan Kartu Hasil Studi</h3>";
		echo "API : get_api('".base_url().'index.php/'.base64_encode('c_api/get_jadwal')."',array('MhswID','TahunID'));";
		echo "<br>";
		echo "API : ".base_url()."'index.php/c_api/get_jadwal";
		
		echo "<h3>Absensi Kehadiran</h3>";
		echo "API : get_api('".base_url().'index.php/'.base64_encode('c_api/get_absensi')."',array('MhswID','TahunID'));";
		
		
		echo "<h1>- API Portal Mahasiswa Mobile Apps- </h1>";
		
		
	}
	
	function login($username='',$password='')
	{
		$this->load->model('users_model');
		
		header('Content-type:application/json');
		header('access-control-allow-origin:*');
		
		if(!$username)
		{
		@extract($_POST);
		$username=htmlentities($nama);
		$password=htmlentities($pas);
		}
		$Q = $this->users_model->cek_login($username);
		if($Q == false)
		{
			$data['status']=0;
			$data['alert']='Maaf username anda salah';
		}
		else
		{
			if($Q->Password != md5($password))
			{
				$data['status']=0;
				$data['alert']='Maaf password salah';
			}
			else
			{
				$data['username'] =$Q->Nama;
				$data['EntityID'] = $Q->EntityID;
				$data['tipeuser'] = $Q->TabelUserID;
				
				if($Q->TabelUserID == 4)
				$akses=2;
				elseif($Q->TabelUserID == 2)
				$akses=3;
				
				$data['aksesid'] = $akses;
				
				$this->db->where('ProsesBuka',1);
				$this->db->order_by('TahunID','DESC');
				$thn=$this->db->get('tahun')->row();
				
				$this->db->where('MhswID',$Q->EntityID);
				$dospem=$this->db->get('setpembimbing')->row();
				
				$data['DosenPembimbingID'] = $dospem->DosenID;
				
				$data['tahunaktif'] = $thn;
				$data['jenisuser'] = get_field($Q->TabelUserID,'tipeuser');
				
				$data_user=get_id($data['EntityID'],$data['jenisuser']);
				$data['data_user']= $data_user;
				
				$data['UserID'] = $Q->ID;
				$data['urlFoto'] = get_photo($data_user->Foto,$data_user->Kelamin,$data['jenisuser']);
				$data['status']=1;
				
			}
		}
		header('HTTP/1.1: 200');
		header('Status: 200');
		header('Content-Length: '.strlen($data));
		
		$result['responseData']['results'][0]=$data;
		
		echo json_encode($result);
		
		exit();
	}
	
	function get_jadwal($MhswID,$TahunID)
	{	
		/*
		header('Content-type:text/json');
		header('access-control-allow-origin:*');
		$_POST;
		*/
		
		$this->load->model('m_rencanastudi','model');
		$this->load->model('m_teamteaching');
		
		$jadwal=$this->model->get_data($limit,$offset,$MhswID,$TahunID);
		
		$data = json_encode($jadwal);
        
		header('HTTP/1.1: 200');
        header('Status: 200');
        header('Content-Length: '.strlen($data));
        exit($data);
	}	
	
	function get_absensi($MhswID,$TahunID)
	{
		$this->load->model('m_rencanastudi','model');
		
		$absensi=$this->model->get_npm($MhswID,$TahunID);
		
		$data = json_encode($absensi);
        header('HTTP/1.1: 200');
        header('Status: 200');
        header('Content-Length: '.strlen($data));
        exit($data);
	}
	
	function form_rencanastudi($MhswID,$TahunID)
	{
		$this->db->order_by('SKS','DESC');
		$Qdata=$this->db->get('rangesks')->last_row();
		
		$data['sks']=$Qdata->SKS;
		
		$mhs=get_id($MhswID,'mahasiswa');
		$data['MhswID'] = $mhs->ID;
		$data['varTahunID'] = $TahunID;
		$data['ProdiID'] = $mhs->ProdiID;
		$data['ProgramID'] = $mhs->ProgramID;
		
	}
}
?>